var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'localhost',
  port : 3308,
  user: 'root',
  password: '',
  database: 'regform',
  multipleStatements: true
});

connection.connect(function(error){
if(!error){
  console.log('success');
}
else{
  console.log(error);
}
});

module.exports = connection;
