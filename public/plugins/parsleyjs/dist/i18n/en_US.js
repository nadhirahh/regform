function translation(moduleName, text) {
	
	switch(moduleName){

		case 'dataTable':
			var dataTableText={
				'LanguageUrl' : "../plugins/datatables/i18n/en_US.json"
			};
				
			if (dataTableText[text]) {
				return dataTableText[text];
			}
			return text;
		break;

		case 'success':
			var successText={				
				'Success':'Success',
				'ItemUpdated':'Item updated successfully.',
				'ItemDeleted':'Item deleted successfully.',
				'Deleted':'Deleted'
			};
				
			if (successText[text]) {
				return successText[text];
			}
			return text;
		break;

		case 'warning':
			var warningText={				
				'Warning':'Warning',
				'SelectItem':'Please select an item.',
				'SelectAtLeastOneItem':'Please select at least one item.',
				'SelectOneItem':'Please select one item only.',
				'DuplicateItem':'Item already exist.',
				'FillReqField':'Please fill in the required fields.',
				'Confirmation':'Are you sure?',
				'ReminderDel':"You won't be able to revert this!",
				'ConfirmDel':'Yes, delete it.',
				'CancelDel':'No, cancel.'
			};

			if (warningText[text]) {
				return warningText[text];
			}
			return text;
		break;

		case 'error':
			var errorText={				
				'Error':'Error',
				'ServerDown':'Cannot connect to server.',
				'NoViewAccess':'No permission to view item.',
				'NoUpdateAccess':'No permission to change this item.',
				'NoSaveAccess':'No permission to save this item.',
				'NoDeleteAccess':'No permission to delete this item.',
				'NoAccessId':'Access Id not found.',
				'Cancelled':'Cancelled.'
			};
				
			if (errorText[text]) {
				return errorText[text];
			}
			return text;
		break;
			
		default:
			return text;
	}
}

console.log('english localization');