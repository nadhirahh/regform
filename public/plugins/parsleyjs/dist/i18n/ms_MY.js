function translation(moduleName, text) {

	switch(moduleName){
		case 'general':
			var generalText={
				'Username':'Nama Pengguna',
				'Password':'Kata Laluan',
				'Repository':'Repositori',
				'Language':'Bahasa',
				'Log In':'Masuk',
				'Log Out':'Keluar',
				'Dark':'Hitam',
				'Green':'Hijau',
				'Material':'Material',
				'Yellow':'Kuning',
				'Purple':'Ungu',
				'Blue':'Biru'
			};
				
			if (generalText[text]) {
				return generalText[text];
			}
			return text;
		break;

		case 'btnLabel':
			var buttonText={				
				'Grid View':'Paparan Grid',
				'List View':'Paparan Senarai',
				'Create New':'Tambah Rekod',
				'Save':'Simpan',
				'View':'Papar',
				'Edit':'Kemaskini',
				'Add':'Tambah',
				'Delete':'Hapus',
				'Remove':'Padam',
				'Close':'Tutup',
				'Cancel':'Batal',
				'Previous':'Sebelum',
				'Next':'Seterusnya',
				'Back':'Balik Semula',
				'Save / Next':'Simpan / Seterusnya'
			};
				
			if (buttonText[text]) {
				return buttonText[text];
			}
			return text;
		break;

		case 'dataTable':
			var dataTableText={
				'LanguageUrl' : "../plugins/datatables/i18n/ms_MY.json",
				'Search':'Carian',
				'Previous':'Sebelum',
				'Next':'Kemudian'
			};
				
			if (dataTableText[text]) {
				return dataTableText[text];
			}
			return text;
		break;

		case 'success':
			var successText={				
				'Success':'Berjaya',
				'Action done successfully.':'Tindakan berjaya dilakukan.',
				'ItemCreated':'Rekod berjaya dicipta',
				'ItemUpdated':'Rekod berjaya dikemaskini.',
				'ItemDeleted':'Rekod berjaya dihapuskan.',
				'Deleted':'Deleted',
			};
				
			if (successText[text]) {
				return successText[text];
			}
			return text;
		break;

		case 'warning':
			var warningText={				
				'Warning':'Amaran',
				'SelectItem':'Sila pilih rekod.',
				'SelectAtLeastOneItem':'Sila pilih sekurang-kurangnya satu rekod.',
				'SelectOneItem':'Sila pilih satu rekod sahaja.',
				'DuplicateItem':'Rekod telah wujud.',
				'FillReqField':'Sila masukkan semua data yang diperlukan.',
				'Confirmation':'Adakah anda pasti?',
				'ReminderDel':"Anda tidak dapat mengembalikan rekod.",
				'ConfirmDel':'Ya, Hapuskan rekod.',
				'CancelDel':'Tidak, Batalkan.'
			};

			if (warningText[text]) {
				return warningText[text];
			}
			return text;
		break;

		case 'error':
			var errorText={				
				'Error':'Ralat',
				'Action failed to complete.': 'Tindakan tidak berjaya',
				'ServerDown':'Tidak dapat menyambung ke pelayan.',
				'NoViewAccess':'Tiada kebenaran untuk melihat rekod.',
				'User already login.':'Pengguna telah Log Masuk.',
				'NoUpdateAccess':'Tiada kebenaran untuk mengemaskini rekod.',
				'NoSaveAccess':'Tiada kebenaran untuk menyimpan rekod.',
				'NoDeleteAccess':'Tiada kebenaran untuk menghapuskan rekod.',
				'NoAccessId':'Tiada Akses Id.',
				'Cancelled':'Batal.'
			};
				
			if (errorText[text]) {
				return errorText[text];
			}
			return text;
		break;
		
		case 'acl':
			var aclText={
				'Access Control':'Kawalan Akses',
				'Full Name':'Nama',
				'User Type':'Jenis Pengguna',
				'View':'Paparan',
				'Update':'Kemaskini',
				'Remove':'Hapus',
				'Modify Access':'Ubah Suai Akses',
				'Add Everyone':'Tetapan Semua',
				'Add Access Control':'Tambah Kawalan Akses',
				'Remove Access Control':'Hapus Kawalan Akses'
			};
			if (aclText[text]) {
				return aclText[text];
			}
			return text;
		break;

		case 'itemType':
			var itemTypeText={				
				'List of Item Type':'Senarai Jenis Item',
				'Details':'Butiran',
				'Access Control':'Kawalan Akses',
				'Settings':'Tetapan',
				'Item Settings':'Tetapan Item',
				'Numbering':'Penomboran',
				'Audit Log':'Log Audit',
				'Item Form':'Borang Item',
				'Inter-Processor':'Inter-Processor',
				'Code':'Kod',
				'Title':'Tajuk',
				'Description':'Keterangan',
				'Remark':'Catatan',
				'Item Level':'Aras Item',
				'Icon':'Ikon',
				'Active':'Aktif',
				'Active Date From':'Tarikh Aktif Dari',
				'Active Date To':'Tarikh Aktif Hingga'
			};
				
			if (itemTypeText[text]) {
				return itemTypeText[text];
			}
			return text;
		break;
		
		case 'docStore':
			var itemTypeText={				

			};
				
			if (itemTypeText[text]) {
				return itemTypeText[text];
			}
			return text;
		break;
		
		case 'roles':
			var rolesText={				

			};
				
			if (rolesText[text]) {
				return rolesText[text];
			}
			return text;
		break;
		
		case 'emailTemplate':
			var emailTemplateText={				

			};
				
			if (emailTemplateText[text]) {
				return emailTemplateText[text];
			}
			return text;
		break;

		case 'warehouse':
			var PhysicaLocationText={				
				'Add User':'Tambah Pengguna',
				'Full Number':'Nombor Penuh',
				'Full Number :':'Nombor Penuh :',
				'Number :':'Nombor :',
				'Type Name :':'Jenis Nama :',
				'Uncompressed Number :':'Nombor Asal :',
				'Compressed Number':'Nombor Dipendekkan',
				'Locked':'Dikunci',
				'Compressed Number :':'Nombor Dipendekkan :',
				'Locked :':'Dikunci :',
				'Uncompressed Full Number :':'Nombor Penuh Asal :',
				'Recalculate Space':'Kira Semula Ruang',
				'Calculate':'Kira',
				'Title Structure :':'Struktur Tajuk :',
				'Description :':'Keterangan :',
				'Type':'Jenis',
				'Available Space :':'Ruang Tersedia :',
				'Total Space :':'Jumlah Ruang :',
				'Item Space':'Ruang Item',
				'Available Space':'Ruang Tersedia',
				'Action':'Tindakan',
				'Security Level Title :':'Tajuk Paras Keselamatan :',
				'Choose at least one space type':'Sila pilih satu jenis ruang',
				'View Child':'Papar Maklumat',
				'Item Unit :':'Unit Item',
				'Title':'Tajuk',
				'Title :':'Tajuk :',
				'Warehouse :':'Gudang :',
				'Floor :':'Aras :',
				'Room :':'Bilik :',
				'Cabinet :':'Almari :',
				'Shelf :':'Rak :',
				'Box :':'Kotak :',
				'Capacity :': 'Keupayaan :',
				'Number Pattern :': 'Corak Nombor :',
				'Start Number After :': 'Mulakan Nombor Selepas :',
			};
				
			if (PhysicaLocationText[text]) {
				return PhysicaLocationText[text];
			}
			return text;
		break;

		case 'application':
			var applicationText={
				'Code :':'Kod :',
				'Title :':'Tajuk :',
				'Description :':'Keterangan :',
				'Previous':'Sebelum',
				'Next':'Seterusnya'
			};
			
			if (applicationText[text]) {
				return applicationText[text];
			}
			return text;
		break;

		case 'customField':
			var customFieldText={
				'String':'String',
				'Integer':'Integer',
				'Decimal':'Decimal',
				'Field Type :':'Jenis Bidang :',
				'Date':'Tarikh',
				'All':'Semua',
				'Name :':'Nama :',
				'Length :':'Panjang :',
				'Text':'Teks',
				'List of Value':'Senarai Nilai',
				'List of Value :':'Senarai Nilai :',
				'Object':'Objek',
				'Multi Line':'Pelbagai Baris',
				'Rows Length :':'Panjang Baris :',
				'Cols Length :':'Panjang Ruangan :',
				'Regex Masking :':'Pelekat Regex :',
				'Multi Value':'Pelbagai Nilai',
				'Multi Value :':'Pelbagai Nilai :',
				'Reference Field Name :':'Nama Bidang Rujukan :',
				'Display Type :':'Jenis Paparan :',
				'Allow Others Value':'Membenarkan Orang Lain',
				'Object Name :':'Nama Objek :',
				'Object Reference List :':'Nama Objek Rujukan :',
				//'Delete':'Nama',
				'Choose....':'Pilih....',
				'Minimum Integer :':'Perpuluhan Minimum :',
				'Maximum Integer :':'Perpuluhan Maksimum :',
				//'Digit :':'Digit :',
				'Minimum Decimal :':'Perpuluhan Minimum :',
				'Maximum Decimal :':'Perpuluhan Maksimum :',
				'Minimum Date :':'Tarikh Minimum :',
				'Maximum Date :':'Tarikh Maksimum :',
				'Custom Type Name :':'Jenis Diubahsuai :',
				'Properties':'Properties'

			};
			
			if (customFieldText[text]) {
				return customFieldText[text];
			}
			return text;
		break;

		case 'securityLevel':
			var securityLevelText={
				'Level No':'Paras No',
				'Level No :':'Paras No :',
				'Description':'Keterangan',
				'Previous':'Sebelum',
				'Next':'Seterusnya'
			};
			
			if (securityLevelText[text]) {
				return securityLevelText[text];
			}
			return text;
		break;
		
		case 'itemNotification':
			var itemNotificationText={				

			};
				
			if (itemNotificationText[text]) {
				return itemNotificationText[text];
			}
			return text;
		break;
		
		default:
			return text;
	}

};

console.log('malay localization');