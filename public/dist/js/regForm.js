
var listStudent= [];
var imgArr = [];
var imgName = [];
var obj = {};
var obj1 = {};

(function($){

      setTimeout(function(){
        
        swal(
            {
                title: "Error",
                text: "Session Expired! Please refresh.",
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                allowOutsideClick: false
            }
        ).then(function(){
            window.location.assign('/');
        });
        
    }, 1800000);
    
    init();
    function init(){

        var form = $("#regBusiness");
        form.steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            labels: {
                previous : 'Sebelum',
                next : 'Seterusnya',
                finish : 'Hantar',
                current : ''
            },
            titleTemplate : '<div class="title"><span class="title-text">#title#</span><span class="title-number">0#index#</span></div>',
            onFinished: function (event, currentIndex)
            {
              submitForm();
            }
    });

    };

    $('input[name="status"]').on('change', null, function(){
      console.log($(this).val());

      if( $(this).val() == 'Sudah Bayar'){
        $('#noRujukan').removeClass('collapse');
        $('#noRujukan_perniagaan').prop('required', true);
      }
      else{
        $('#noRujukan').addClass('collapse');
        $('#noRujukan_perniagaan').prop('required', false);
      }
    });

  
    $('.jumlahJuadah').on('change', null, function(){
      var total = parseInt($(this).val());
      $("#nama_juadah").empty();
      for(var i=0; i< total; i++){
      divs = "";
      divs = divs + '<div class="mb-0">';
      divs = divs + '<div class="row">';
      divs = divs + '<label class="control-label mb-0 transLang" for="juadah">';
      divs = divs + '<input type="file" title="Pilih Gambar" class="custom-file-input form-control form-control-sm Juadah" name="gambarJuadah'+i+'" id="juadah'+i+'" multiple>';
      // divs = divs + '<img id="image_juadah" src="">';
      divs = divs + '</label>';
      divs = divs + '<input type="text" name="gambarJuadah'+i+'" id="nama_juadah'+i+'" class="form-control form-control-sm nama_juadah" placeholder="Nama Juadah" required/>';
      divs = divs + '</div>';
      divs = divs + '</div>';
      $("#nama_juadah").append(divs);
      }

      $('.nama_juadah').each(function(){
        obj = {};
        obj.inputID = $(this).attr('id');
        obj.name = $(this).attr('name');
        imgName.push(obj);
      });

      $('.Juadah').each(function(){
        obj1 = {};
        obj1.imgID = $(this).attr('id');
        obj1.name = $(this).attr('name');
        imgArr.push(obj1);
       
      });
      
    });
    $('.btnRegForm ').click(function(){
        $('#divRegForm').removeClass('collapse');
        $("#divViewList").addClass('collapse');
    });

    var imgJuadah = [];

    $('input[name="gambarBanner"]').on('change', null, function(e){
      var file = e.target.files;
      var formData = new FormData();
      $.each(file, function(i, val){
        console.log(val);
        formData.append('files', val);
      });

      $.ajax({
        url: '/images',
        type: 'POST',
        processData: false,
        // cache: false,
        enctype: 'multipart/form-data',
        // contentType: false,
        data: formData
      }).done(function (data) {
          console.log(data);
      });
    });

    function submitForm(){
      var juadahName = [];
      var bannerName = [];
      var totalImgName = [];
      var temp = [];
      $('#regBusiness').parsley().whenValidate().done(function(){
      $.each(imgName, function(is, val){
        juadah = {};
        juadahName.push($('#'+val.inputID+'').val());
        $.each(imgArr, function(idx, value){
          console.log(value.name + ' - ' + val.name);
          var fileBanner = $('#'+value.imgID+'').get(0).files;
          if(value.name == val.name){
            temp = [];
          $.each(fileBanner, function(i,values){
            // console.log(juadahName);
            temp.push(values.name);
            });
            totalImgName.push(temp);
          }
          });
      });
      console.log(totalImgName);
      var fileBan = $('#banner').get(0).files;
      for(var i=0; i<fileBan.length; i++){
        bannerName.push(fileBan[i].name);
      }
      
      var nama = $('#nama').val();
      var ic = $('#ic').val();
      var phoneNo = $('#phoneNo').val();
      var namaPerniagaan = $('#nama_perniagaan').val();
      var noRujukan = $('#noRujukan_perniagaan').val();

      var paramData = {};
      paramData.name = nama;
      paramData.ic = ic;
      paramData.phoneNo = phoneNo;
      paramData.status = $('input[name="status"]').val();
      paramData.juadah = totalImgName;
      paramData.juadahName = juadahName;
      paramData.bannerName = bannerName;
      paramData.namaPerniagaan = namaPerniagaan;
      paramData.noRujukan = noRujukan;
      console.log(paramData);

      $.ajax({
          url: '/regForm',
          type: 'POST',
          enctype: "multipart/form-data",
          contentType: 'application/json',
          data: JSON.stringify(paramData)
        }).done(function (data) {
            console.log(data);
            swal(
              {
                  title: "Berjaya",
                  text: "Pendaftaran Berjaya!",
                  type: 'success',
                  confirmButtonClass: 'btn btn-confirm mt-2',
                  allowOutsideClick: false
              }
              )
        });
      }).fail(function(){
        swal(
          {
              title: "Ralat",
              text: "Sila isi maklumat yang diperlukan!",
              type: 'error',
              confirmButtonClass: 'btn btn-confirm mt-2',
              allowOutsideClick: false
          }
          )
      });
    };

})(jQuery);
