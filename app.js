const express = require('express');
const path = require('path');
const routes = require('./routes/index');
const bodyParser = require('body-parser');


const app = express();

// app.use(session({key: 'user', secret: 'ssshhhhh',saveUninitialized: true,resave: true}));
app.use(bodyParser.json());      
app.use(bodyParser.urlencoded({extended: true}));
// app.use(fileUpload());

// Set the view engine to html
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

app.listen(8001, () => {
  console.log('Server is running at localhost:8001');
});
