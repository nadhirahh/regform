var express = require('express');
var router = express.Router();
const connection = require('../public/js/script');
var multer = require('multer');
var Storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, 'uploads/');
  },
  filename: function (req, file, callback) {
    console.log(file);
    callback(null, file.originalname);
  }
});

var upload = multer({ storage : Storage }).array('gambarBanner',2);

router.post('/images', function(req, res, next){
upload(req, res, function(err) {
    if (err) {
    res.send(err);
    }
    else{
      console.log(req.files);
      res.send('try success');
    }
  });
});

router.post('/regForm', function(req, res, next) {
  var data = req.body;
  var juadahName = data.juadahName.toString();
  var bannerName = data.bannerName.toString();
  var juadah = JSON.stringify(data.juadah);
  console.log(data);
  console.log(juadah);
  var sql = "INSERT INTO regform (name, ic, phoneNo, noRujukan, juadah, juadahName, banner, namaPerniagaan, status) VALUES(?,?,?,?,?,?,?,?,?)";
  connection.query(sql, [data.name, data.ic, data.phoneNo, data.noRujukan, juadah, juadahName, bannerName, data.namaPerniagaan, data.status], function(error, results, fields){
    if(error){
      res.send(error);
      }
      else{
      res.send(results);
      }
    });
});

router.get('/Form', function(req, res, next) {
  var sql = "SELECT * FROM regform";
  connection.query(sql, function(error, results, fields){
    if(error){
      res.send(error);
    }
    else{
    res.send(results);
    }
  });
});

router.get('/regForm', function(req, res, next) {
  res.render('regForm');
});

module.exports = router;
